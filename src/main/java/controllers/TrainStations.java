package controllers;

import repositories.interfaces.ITrain;

import java.util.ArrayList;
import java.util.List;

public class TrainStations {
    private List<ITrain> trains;

    public TrainStations(){
        trains = new ArrayList<>();
    }

    public void addTrain(ITrain train){
        trains.add(train);
    }

    public void startWork(){
        trains.forEach(t -> t.drive());
    }
}
