package repositories.interfaces;

public interface ITrain extends IWagon {
    void drive();
}
