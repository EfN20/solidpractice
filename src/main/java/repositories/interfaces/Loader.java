package repositories.interfaces;

import domain.Freight;

public interface Loader {
    void loadFreight(Freight freight);
    double allWeight();
}
