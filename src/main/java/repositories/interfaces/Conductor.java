package repositories.interfaces;

import domain.Passenger;

public interface Conductor {
    void addPassenger(Passenger passenger);
    int getNumberOfPassengers();
}
