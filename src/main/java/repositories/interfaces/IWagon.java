package repositories.interfaces;

public interface IWagon {
    String getSerialNumber();
    void setSerialNumber(String serialNumber);
}
