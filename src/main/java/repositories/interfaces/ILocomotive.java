package repositories.interfaces;

public interface ILocomotive extends IWagon {
    void providePower();
}
