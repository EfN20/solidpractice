package repositories.interfaces;

public interface IPassenger extends Conductor {
    void carryPassengers();
}
