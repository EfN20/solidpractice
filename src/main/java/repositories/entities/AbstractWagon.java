package repositories.entities;

import domain.Passenger;
import repositories.interfaces.Conductor;
import repositories.interfaces.ITrain;
import repositories.interfaces.Loader;

import java.util.ArrayList;

public abstract class AbstractWagon implements ITrain {
    private String serialNumber;

    public AbstractWagon(String serialNumber){
        setSerialNumber(serialNumber);
    }

    @Override
    public void setSerialNumber(String serialNumber){
        this.serialNumber = serialNumber;
    }

    @Override
    public String getSerialNumber(){
        return serialNumber;
    }
}
