package domain.trains;

import domain.Passenger;
import repositories.entities.AbstractWagon;
import repositories.interfaces.ILocomotive;
import repositories.interfaces.IPassenger;
import repositories.interfaces.IRestoran;

import java.util.ArrayList;

public class LuxTrain extends AbstractWagon implements ILocomotive, IPassenger, IRestoran {
    private ArrayList<Passenger> passengers;

    public LuxTrain(String serialNumber) {
        super(serialNumber);
        passengers = new ArrayList<>();
    }

    @Override
    public void drive() {
        providePower();
    }

    @Override
    public void providePower() {
        carryPassengers();
        sellFoodAndWater();
    }

    @Override
    public void carryPassengers() {
        System.out.println();
        System.out.println("---------------------------------------------------------");
        System.out.print("This is a Luxury Trains with serial number: " + getSerialNumber() +
                ", it can carry many passengers and provide enough power to drive for a distances. " +
                "At the moment carrying number of passengers is: " + getNumberOfPassengers() + ".");
    }

    @Override
    public void sellFoodAndWater() {
        System.out.println("Also there are Restoran wagons where you can buy food with drinks.");
        System.out.println("---------------------------------------------------------");
        System.out.println();
    }

    @Override
    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    @Override
    public int getNumberOfPassengers() {
        return passengers.size();
    }
}
