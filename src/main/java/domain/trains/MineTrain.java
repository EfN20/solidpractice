package domain.trains;

import domain.Freight;
import domain.Passenger;
import repositories.entities.AbstractWagon;
import repositories.interfaces.IFreight;
import repositories.interfaces.ILocomotive;
import repositories.interfaces.IPassenger;

import java.util.ArrayList;

public class MineTrain extends AbstractWagon implements ILocomotive, IPassenger, IFreight {
    private ArrayList<Freight> freights;
    private ArrayList<Passenger> passengers;

    public MineTrain(String serialNumber) {
        super(serialNumber);
        freights = new ArrayList<>();
        passengers = new ArrayList<>();
    }

    @Override
    public void drive() {
        providePower();
    }

    @Override
    public void providePower() {
        carryFreights();
        carryPassengers();
    }

    @Override
    public void carryFreights() {
        System.out.println();
        System.out.println("---------------------------------------------------------");
        System.out.println("This is a Mine Trains with serial number: " +
                getSerialNumber() + ", and it can carry a lot of freight and provide enough power to drive with" +
                "some weight. At the moment carrying weight is: " + allWeight() + ".");
    }

    @Override
    public void carryPassengers() {
        System.out.println("This is a Mine Trains with serial number: " + getSerialNumber() +
                ", it can carry passengers and provide enough power to drive for a some distances. " +
                "At the moment carrying number of passengers is: " + getNumberOfPassengers() + ".");
        System.out.println("---------------------------------------------------------");
        System.out.println();
    }

    @Override
    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    @Override
    public int getNumberOfPassengers() {
        return passengers.size();
    }

    @Override
    public void loadFreight(Freight freight) {
        freights.add(freight);
    }

    @Override
    public double allWeight() {
        double sumWeight = 0;
        for(int i = 0; i < freights.size(); i++){
            sumWeight += (freights.get(i)).getWeight();
        }
        return sumWeight;
    }
}
