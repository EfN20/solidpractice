package domain.trains;

import domain.Freight;
import repositories.entities.AbstractWagon;
import repositories.interfaces.IFreight;
import repositories.interfaces.ILocomotive;

import java.util.ArrayList;

public class FreightTrain extends AbstractWagon implements ILocomotive, IFreight {
    private ArrayList<Freight> freights;

    public FreightTrain(String serialNumber) {
        super(serialNumber);
        freights = new ArrayList<>();
    }

    @Override
    public void drive() {
        providePower();
    }

    @Override
    public void providePower() {
        carryFreights();
    }

    @Override
    public void carryFreights() {
        System.out.println();
        System.out.println("---------------------------------------------------------");
        System.out.println("This is a Freight Trains with serial number: " +
                getSerialNumber() + ", and it can carry a lot of freight and provide enough power to drive with" +
                "massive weight. At the moment the carrying weight is: " + allWeight() + ".");
        System.out.println("---------------------------------------------------------");
        System.out.println();
    }

    @Override
    public void loadFreight(Freight freight) {
        freights.add(freight);
    }

    @Override
    public double allWeight() {
        double sumWeight = 0;
        for(int i = 0; i < freights.size(); i++){
            sumWeight += (freights.get(i)).getWeight();
        }
        return sumWeight;
    }
}
