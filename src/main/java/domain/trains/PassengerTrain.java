package domain.trains;

import domain.Passenger;
import repositories.entities.AbstractWagon;
import repositories.interfaces.ILocomotive;
import repositories.interfaces.IPassenger;

import java.util.ArrayList;

public class PassengerTrain extends AbstractWagon implements ILocomotive, IPassenger {
    private ArrayList<Passenger> passengers;

    public PassengerTrain(String serialNumber) {
        super(serialNumber);
        passengers = new ArrayList<>();
    }

    @Override
    public void drive() {
        providePower();
    }

    @Override
    public void providePower() {
        carryPassengers();
    }

    @Override
    public void carryPassengers() {
        System.out.println();
        System.out.println("---------------------------------------------------------");
        System.out.println("This is a Passenger Trains with serial number: " + getSerialNumber() +
                ", it can carry many passengers and provide enough power to drive for a distances. " +
                "At the moment carrying number of passengers is: " + getNumberOfPassengers() + ".");
        System.out.println("---------------------------------------------------------");
        System.out.println();
    }

    @Override
    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    @Override
    public int getNumberOfPassengers() {
        return passengers.size();
    }
}
