package domain;

public class Passenger {
    private static int ticketID = 1;
    private String name;
    private String surname;
    private int passportID;

    public Passenger(String name, String surname, int passportID){
        generateTicketID();
        setName(name);
        setSurname(surname);
        setPassportID(passportID);
    }

    public int getTicketID() {
        return ticketID;
    }

    public void generateTicketID() {
        this.ticketID = ticketID++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPassportID() {
        return passportID;
    }

    public void setPassportID(int passportID) {
        this.passportID = passportID;
    }

}
