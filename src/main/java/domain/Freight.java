package domain;

public class Freight {
    private static int productID = 1;
    private String owner;
    private String type;
    private double weight;

    public Freight(String owner, String type, double weight){
        generateProductID();
        setOwner(owner);
        setType(type);
        setWeight(weight);
    }

    public void generateProductID(){
        this.productID = productID++;
    }

    public int getProductID(){
        return productID;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
