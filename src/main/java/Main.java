import controllers.TrainStations;
import domain.Freight;
import domain.Passenger;
import domain.trains.FreightTrain;
import domain.trains.LuxTrain;
import domain.trains.MineTrain;
import domain.trains.PassengerTrain;

public class Main {
    public static void main(String[] args) {
        TrainStations mainStation = new TrainStations();

        PassengerTrain passengerTrain = new PassengerTrain("Qwe123");
        passengerTrain.addPassenger(new Passenger("Rakhat", "Omarali", 123));
        passengerTrain.addPassenger(new Passenger("Ansar", "Shilibekov", 132));
        mainStation.addTrain(passengerTrain);

        FreightTrain freightTrain = new FreightTrain("Qwe124");
        freightTrain.loadFreight(new Freight("Rakhat", "Meat", 20.75));
        freightTrain.loadFreight(new Freight("Ansar", "Stone", 100.00));
        mainStation.addTrain(freightTrain);

        MineTrain mineTrain = new MineTrain("Qwe125");
        mineTrain.addPassenger(new Passenger("Nurmukhamed", "Sherkeshbay", 228));
        mineTrain.loadFreight(new Freight("Nurmukhamed", "gold", 1337.00));
        mainStation.addTrain(mineTrain);

        LuxTrain luxTrain = new LuxTrain("Qwe126");
        luxTrain.addPassenger(new Passenger("Azatkali", "Nurumgaliyev", 880));
        mainStation.addTrain(luxTrain);

        mainStation.startWork();
    }
}
